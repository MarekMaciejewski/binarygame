import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SolutionBulkTest {
    private int leap;
    private int[] game;
    private boolean expected;

    private static List<Object[]> testCases = new ArrayList<>();

    static {
        Scanner scanInput = null;
        Scanner scanOutput = null;

        try {
            FileInputStream inputFile = new FileInputStream ("src\\main\\resources\\testInput");
            scanInput = new Scanner (inputFile);
            FileInputStream outputFile = new FileInputStream ("src\\main\\resources\\testOutput");
            scanOutput = new Scanner (outputFile);
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("File was not found!");
        }

        int q = scanInput.nextInt();

        while (q-- > 0) {
            int n = scanInput.nextInt();
            int leap = scanInput.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scanInput.nextInt();
            }

            boolean exp = scanOutput.nextLine().equals("YES");

            Object[] testUnit = new Object[]{leap, game, exp};

            testCases.add(testUnit);
        }

        scanInput.close();
        scanOutput.close();
    }

    @Parameters
    public static List<Object[]> data() {
        return testCases;
    }

    public SolutionBulkTest(int leap, int[] game, boolean expected) {
        this.leap = leap;
        this.game = game;
        this.expected = expected;
    }

    @Test
    public void canWinTest() {
        boolean actual = Solution1.canWin(leap, game);
        assertEquals(expected, actual);
    }
}
