import java.util.HashSet;
import java.util.Set;

class Move {

    private GamePlan gamePlan;
    private int currentPosition;
    private static Set<Integer> forbiddenPositions = new HashSet<>();

    private Move(GamePlan gp, int curPos) {
        this.gamePlan = gp;
        currentPosition = curPos;
    }

    Move(GamePlan gp) {
        this.gamePlan = gp;
        currentPosition = 0;
        forbiddenPositions.clear();
    }

    boolean isNextMovePossible() {
        return !forbiddenPositions.contains(currentPosition)
                && ((gamePlan.getLeap() > 0
                        && isNextStepPossible(gamePlan.getLeap()))
                    || isNextStepPossible(1)
                    || (currentPosition > 0 && isNextStepPossible(-1)));
    }

    private boolean isNextStepPossible(int step) {
        return isGameWon(step)
                || (gamePlan.getGame()[currentPosition + step] == 0
                        && isNextStepInNextMovePossible(step));
    }

    private boolean isGameWon(int step) {
        return currentPosition + step >= gamePlan.getGame().length;
    }

    private boolean isNextStepInNextMovePossible(int step) {
        if (step < 0) forbiddenPositions.add(currentPosition);
        Move nextMove = new Move(gamePlan, currentPosition + step);
        return nextMove.isNextMovePossible();
    }
}
