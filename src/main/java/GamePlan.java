final class GamePlan {
    private final int[] game;
    private final int leap;

    GamePlan(int[] game, int leap) {
        this.game = game;
        this.leap = leap;
    }

    int[] getGame() {
        return game;
    }

    int getLeap() {
        return leap;
    }
}
