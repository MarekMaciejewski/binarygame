import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

class Solution2 {
    static boolean canWin(int leap, final int[] game) {
        // Return true if you can win the game; otherwise, return false.
        
        boolean moveFwd = true;
        boolean leapNotZero = true;
        if (leap == 0) leapNotZero = false;
        Set<Integer> set = new HashSet<>();
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < game.length;) {

            if (i >= game.length - leap || i == game.length - 1) {
                return true;
            }

            if (!stack.empty() && stack.peek() == i + leap) break;

            if (leapNotZero && game[i + leap] == 0 && !set.contains(i)) {
                moveFwd = true;
                i += leap;
                stack.push(i);
                continue;
            }

            if (moveFwd && game[i + 1] == 0) {
                i++;
                continue;
            }

            if (!stack.empty() && moveFwd) i = stack.peek();

            if (i > 0 && game[i - 1] == 0) {
                moveFwd = false;
                i--;
                continue;
            }

            if (!stack.empty()) {
                moveFwd = true;
                i = stack.pop() - leap;
                set.add(i);
                continue;
            }

            break;
        }

        return false;
    }
}