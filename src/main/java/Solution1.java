class Solution1 {
    static boolean canWin(int leap, int[] game) {
        // Return true if you can win the game; otherwise, return false.
        GamePlan gp = new GamePlan(game, leap);
        Move move = new Move(gp);
        return move.isNextMovePossible();
    }
}
