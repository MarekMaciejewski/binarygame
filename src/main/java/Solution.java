import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scan = null;
        try {
            FileInputStream textFile = new FileInputStream ("src\\main\\resources\\testInput");
            scan = new Scanner (textFile);
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("File was not found!");
        }
        //Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (Solution1.canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
    }
}
